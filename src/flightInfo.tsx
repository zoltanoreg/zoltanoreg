import {
    Card,
    CardContent,
    CardHeader,
    Divider,
    Grid,
    LinearProgress,
    Typography,
} from '@material-ui/core'
import moment from 'moment'
import * as React from 'react'
import styled from 'styled-components'

interface IFlightInfo {
    flightData: IFlightData
    text: IText
}

interface IFlightData {
    destinationIATA: string
    distanceToDestination: number
    distanceToOrigin: number
    originIATA: string
    timeToDestination: number
}

interface IText {
    title: string
    subtitle: string
    from: string
    to: string
    arriving: string
    distance: string
}

export class FlightInfo extends React.Component<IFlightInfo> {
    public getProgressState = (flightData: IFlightData): number =>
        Math.round(
            (flightData.distanceToOrigin /
                (flightData.distanceToDestination + flightData.distanceToOrigin)) *
                100
        )

    public render(): React.ReactNode {
        const { flightData } = this.props
        const timeToDestination: string = moment
            .duration({ minutes: flightData.timeToDestination })
            .humanize()

        const FlightCard = styled(props => <Card {...props}>{props.children}</Card>)({
            borderRadius: 10,
            width: 300,
        })

        const FlightCardHeader = styled(props => (
            <CardHeader {...props}>{props.children}</CardHeader>
        ))({
            '& span': {
                position: 'absolute',
                padding: 5,
                color: 'rgb(0, 233, 255)',
            },
            '& span:first-child': {
                top: 0,
                left: 0,
            },
            '& span:nth-child(2)': {
                right: 0,
                bottom: 0,
            },
            backgroundImage:
                'url(data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxANDw0PDxAQDQ0NDQ0NDQ0NDw8NDw0NFREWFhURFRUYHSggGCYmHRUVITEtJSk3Oi4uFx83ODM4NygtLisBCgoKDQ0NFQ0NFSsZFR0tLS0tKysrLSstKy0rKysrLS0tKy0tLSsrKy0tLSsrLSstLSstLTcrLS0rKy0tKy03K//AABEIAKgBLAMBEQACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAAAAQIDBgUEB//EADYQAAIBAwIEBAQEBAcAAAAAAAABAgMREgRhBRMhUQYxQXEigZGhFFKx8DKCktEHIzNiY3LB/8QAGgEBAQEBAQEBAAAAAAAAAAAAAAECAwQFBv/EACoRAQEBAAICAgEDBAEFAAAAAAABEQISAyEEMVFhcYETIjJBFEJSkdHh/9oADAMBAAIRAxEAPwD2eUfO7P0GFyxphcsaYl9DUyozmkyy4YxlQXsbnKs2M3Qfp1NTnDGUqbXmiyys5UWNGDEaYeJNMGI0wYjQ8BsMJxGwwYMbDBgxsMGD7DYYHFjTCsUwgYLBBYAsNBYauCw0IamCxdMAMIJgsFKwBYqYLBTCYVgpMaiWi6Oj5h8/HoCqjApTQkGblsaiJvcvswYDTCVIaYpwXYktMfPPTLsdJ5EvFP4cvdOqXRL2MUqF/Qz3MH4YdzqPwzL3h1P8PYndepOg+wnM6lyWXtEwKmxq4eJNMLAdqmDkl7Uwvww7nUfhty9zqqGnRm86SLdAndeo5Ow7HVM6KLOVSxi6aNzlUwsEXaZCaQ2noniX2npDkizUS2VCuAAA0JlElMdJbY+dr0YlwQ2mFyy9jBgNMJwQ0xDTLKYXUuxMVGJm1cWoImmE6aL2phKmheVMaxsZtrWDFe42mHZdhtMCgiaYHDYaYylTuanLExEqBqc0xDpF7mDk/uw7mFKlb1+wnPUxOK3+hrTD6b/YexLkiiXUa9fuMiWs51ZGpIltZOT7m5Iz7QyollQhoVi6ENAEBVwAwyGEVCxGjpEfO16sOxNVLLqYl3KpYsbiYOWTsYfLJ2MGBdUNDRSgTQ8CaDlDRSgTQ8RoVhoYMJoGFgXRnOkyywYzUjcsZ9s7M1sTEyiyywys5XNyxKhllZKwBYBWRfYVkNoTSLtTInFF2mRLiiypiXEuphYl0wWGoLDQWGgsNCsB0dj52vUVhoLMAtsA7kAAYjVwYjTFqJLRSS7GdZ9quhplFxp7A0A0+jwuNTRykNTsLJDau2joNPZWGrLUOFyy4us3RLOYh0TU5LjGdFdzU51mx886e51nJmxm18zWpiSypiWVEMBBAalUF2IRQAwDTCsEFigsEwrDVx02B8vXfSwGmjAabD5Y07Dlom07HghtNpYoabR0GnswoALBDsTU07DQhqGUO4MImnsdCnsrhcqXMLIhyDUiZFlXGM4+xuXUx881+7nSVmxk4mtSxLgalTEuBqUxDiWWM4loumCwQrAKxdBYaAuoVhoY0FhpgsXTHS2PmOugICaAaoJoC6elKJNS0+Whqdxyxp2HLJp2GA07DAaaXLGnYsBq9oWI1ZQNUi6YBqkNCsNCsNNQ47FlXWbiuxZauIcF2NzlTEOmtjXZMZyp7o1OSYhw3RqcksZSgalZsS0alMKxdQrF1MS0NMBdQrAABYAAAjoj5uu2C40w8iaYMiGGmNTFJjUUialUmNZq1IazYd0D2TsCaljWisNCsNCaJqyliXWpSsNNGI1dLAaaTgXV1OI0lLAauolTNSrKiVMsrWxnKBqUxlKluzc5xMQ6P7uWc4ljOVNr1RucpUsZG2cIagLphWLtQsRqYTiXTCsXUIaHYaAaOgzPmO2FkFw7hDRNFXIguExSCUxqHcamFfcauBMLhjWfR2Gmw7E1NCiNNUojU0YF07HgNTUyihrUtrOVkNbm1lKaDc41DqGpFxLqbjFkiZVCyL6iHUNSGoczWIzm+hqQr5pNv2OkyMW1FjfZANSwhoC6AaYQQWKCw1MFhpgsNMe9dHzdrp7P4e5NpdUse41Lv4UsSbU9h27jTaEtxpo+Y0FkNNFhqaeI01WKGptUiJdUvYah/IayCaGNCc13Gk42s5VS63ODGTErpJIhxNa1LiXHcurpOmu5Oy6TpGuxKnlDsuxMoFlX0yktjcqVDRrUJouphYoaYTgizlT08vinGdLpE+dWhGSV+WvjqP8AlXU7cOHPl9Rw8nm8fD/Kvm4Xxl6rF09NXjSlJx51VQpwSs3kle77dF6m+fCcfuzWeHm7/XG5+XrOJyldsKxdQiygLoBqAo9dRPm67w0hpppE1KdhqKRKilYmp7FwYaZNTFXGplNMamKTGpYrK3qNTNDqoe06UucPa/0ydYi9Cc7gkwhqpZdWERrCb2LDB+/Is0fnPG/H1Sd4aSHJV2ubVSlO20PKPzufT8Xw+M987/Dw+T5d+uEc7HxbxCMr/iZy2lGm4/TE9f8AxvDZnV5/+T5Zd16mk/xE1EbKtSp1l6uDlSl/6jly+Fwv1cdePz+c/wApr39D490dXpU5mnl/yRyj/VG/3PPy+F5J9e3p4fP8d+/X7vb0vEqOoV6NWnVX+yal9vM43x8uP+Ux6OPl48vfG6369jOxt8PEeK0dKr1qtOn2jKXxP2iurOvDxcud/tmuXPzcOE/uuOX1/j6CutNRlWf56l6cFvbz/Q9XD4f/AH3Hi5/Pn/RNeBW4xxHX5KLqOmv446SLjCPX1kuv3fseiePxeP8A+vLy83n8v19fo9rg2g0Wiq0qOpUJ8QqSVkuZXjCUn8K8rRfucfJz8nPjbx/xn8PR4vH4vHynHn753+XaHi19H6KxqUFhqYWJdTCsJTBYumCw0x699z5266hyXcvsws0MphcxDKYaqLYmGDmbjDIanuMMPmbkxMNVNxhgz3GGHmMMgyBguA0yIpDRRNZAQxoBolsq+yuPa5XN8e8IafWN1I309aXVzppOM33lH+1j1eL5fPhMvuOHk+Nx53fquO4h4C1dO7punXj6Yy5cn8pdPue7h87x379PJz+H5J9e3Oa3hOoof6tCrT3cJY/1Lp9z18fLw5fV15ufi5z7jzztHEl0aa6SXlJdGvZj19U9y7H2R41q1FwWpr4PzXOn+t7mP6Xjt3I1/W8kmdr/AOXxxn1cpLOT9ZN9X3b82dM9ZPTG+9vt0/h6hwxunLUaiTmm3OhXhytO210Sa6dH3fXseXzXzzZw4/y9fg4+C2Xny/j/AE6bxJ4no6KjGlpXTlWnH/LVLF06MPzu3T2R5fB8fl5OXbybj2ef5PDx8evjzf0ct4C00tRr+bNubowqV5zk7uVSXwq7/mb+R6vlcpw8WT/fp4vh8bz83a+89v0+crK7skvNvoj5cfZtk+yjJSSaakn5NO6fzHs+yaLKJZrUwioAAI+t1NzyZjtpZblNF9x/CF07j3+EVddye1PLcLp5ETRmMNNMYapMlVRnUUmLUq0ZRaJ7DRNqapE1nTG00yahF9qTG1ZUsS1dZs1LViGalajjPGnibUaGtTp0oQ5cqSm51YSkpycpJxTuvJJfU+j8TwcPJwvLlff6PH8nz8vHZOMcDxjiT1c+ZKlSpTaSm6MXFVH+Zq/n/Y+n4vH045Lb+75nl8ne7ZP4Y6ThlfUY8qjUqZtxjJRahKSTds38Po/X0NcvJw4/5XE4+Lny+pr7tT4R19OLk9PJxSu8JQqNL/qnd/I58fl+K3JyjfL4nmk3q+bUeHtZSSc9LWSaveMOZ9cb2+ZufI8XK5OUYvx/Lxm3jXwy09RedOotnCa/VHTtxv8Atz6cvwmFKTeMYycvyRi3L6LqO0ntJxt+o97gNDiFFyjQp16Ma8qcatTkfFGKb8s7fmZ5/Ny8Nn99nr/T0+Lj5uN/tlm/7d1p/DdFPKvKrran5tVUdSN9ofwr6Hz+XyOV9cc4z9H0uPxuM9895fv/AOnrxSikkkorolFJJL2OVtt2u8kkyDIGk5CFpXNSs6Vy6adxprS553QKQQ7kU0NFIaGkS0NImikiauKVu5LauKViexSsZ2ilYltZaKxm2pVontKtIm1lSQ1Kqxnamiw2posNXUssqyoZpqM5MsWM3I1GoxrQjNWlGMl2klJfRmuPLlx9y4vWWZZrw63hTQznOboR+OLjKKcoxTbvlFJ/C/Y9U+Z5pJNcb8XxW7j2KaUYqMbKMUkkrJJLY4W23b9u8kknGHfcjROW5URKa7o1NZuVlLFNySWTVnJJJte5uW5lvpnJuyeyciyCblCuWAuVCuGRcBXLoLhDyZz9NexkS40aY0WiCiaKRm1qRaRm0xaRm1ZFJbGdXFJbE39TFoztMWiWs1SGpWiJeTNWkZ7MVSROzOrSJeRpCUS0yywZyRqWNRnI1LFjOZuY1GMjUxWbbNemvaG2amHtPUbD2Q2HtNty7EwrF0wWLpgcRokuphF0A2gsXUxLsUyDoPaYC6IyMqdyWKeRBSmTBopkqqUjGKpTJYq1IzY1KtTM2C1IyKTJTGkWYrNaRkSsWNFIzWbF5GcrOGpovWpYeaJ1qYMhIiZSNYrOUjUixm5mpG4zlI1I1GMpmpFmspyNzF9snJmoJcjUwK4mCS7EJsvo9l1L6CdwhFMFywK5ZgVy+k9lcsS6VyodwA5tmTTDJq4aexNXFq/YmmLRm0ikTVymiWrIpe5m1cUpLuT+A1UF4jSNQxYljSNQzYljRTM2M2LUifTNi0xazikyamC5NTCbGriJSLFkZykamtSMpSNzWpGMpGo3Iyczcis3M1IE6hZBEpmpETzGXEJ1GXE1LmXE0syyGlmXEGRcE5mpIFkMZtpZFT2LgGQRat3OddMUmu5m7+Gpn5Ga7ky3/S+vypVdydf0XZ+T577jpDT5rJ1hpqqOsU1MmQNSJSKUiCkyVcaRkYpjSMjNZsaxkZsSxpGZis40UjKYeRn2YeYxMJyNYYzlI1IuMpyNyNSMpSNRZGM5m5rUjGUzcgzcjUEuRqGE2WBNlSwrhlNzUiJci4hXKE5CQJyNSIFIYyeRcCchIJyNYy0OTrhhcBNXDsZ1ZFRWzJqzipLZmdjXVS9mS0kWr9jNq4aZDFK5m2Lhi2GLizFMWpEpjSMzFSxpGZmxnqtTM2JeKshidRkEwnIsq4hvcsXGcnubiyMJtG5qyMptG5oyc0bkq+kOZqcaiXMvWol1DUlS2FzC4anMsjNpOZZE1OZrELIdQnM1Imk6hcTSzLiaMy4mhzGGpuXEbKszl0duyucZvBqcjVdmb41nJS1D7mb42pzWtSzN8Szmpaj2M3xNd1LUexn+kvZXP9iXxr2Pne31J0NUqvsZvFVKoZvH9VUqmxm8f1MWpmLxv5FqZmzlGbFqa7MzZUsUpLcz/cmKyW49s9aLrcezqTkiy1cRKSNS8l6spNdzpOXL8L1ZSt3Ny8vwdYxlbudZb+DrGUku5uW/g6xLsalqWRDNaxYlmomRLNSpYCxMSaZS2WJiWygLESWILlxKWRcSk5DGSyKBTJY6SnmMaPIzYGpDFilIzY1FKRnFi0zNiqUjONSKTM2NRSZmtYpMzcJFp7mbjWVSkYv7Li1Lczf2MWpnO7+F6rUzP8J1PPcmfodTzH8J1JzLP2OqHLc1L+h1ZyZ0lOrOT3OkTqykzcpiGblSxLLGcS2ajNiWzUjKXIsiVLkakZJyLIhZGsROReoMi5EpZDGSuawJssjNJyEiJyNYa//Z)',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            position: 'relative',
            height: 50,
        })

        const FlightLinearProgress = styled(props => (
            <LinearProgress {...props}>{props.children}</LinearProgress>
        ))({
            '& div': {
                backgroundColor: 'rgb(0, 233, 255)',
            },
        })

        const FlightInfoGrid = styled(props => <Grid {...props}>{props.children}</Grid>)({
            textAlign: 'center',
        })

        const CardDivider = styled(props => <Divider {...props}>{props.children}</Divider>)({
            backgroundColor: 'rgba(0, 233, 255, .4)',
        })

        return (
            <FlightCard>
                <FlightCardHeader
                    title={this.props.text.title}
                    subheader={this.props.text.subtitle}
                />
                <CardContent>
                    <Grid container spacing={24} alignItems="center">
                        <Grid item xs={3}>
                            <span className="flightdata-label">{this.props.text.from}</span>
                            <Typography gutterBottom component="p">
                                {flightData.originIATA}
                            </Typography>
                        </Grid>
                        <Grid className="flight-info-progress-bar-container" item xs={6}>
                            <FlightLinearProgress
                                variant="determinate"
                                value={this.getProgressState(flightData)}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <span className="flightdata-label">{this.props.text.to}</span>
                            <Typography gutterBottom component="p">
                                {flightData.destinationIATA}
                            </Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardDivider variant="middle" />
                <CardContent>
                    <FlightInfoGrid container spacing={24}>
                        <Grid item xs={12}>
                            {this.props.text.arriving} {timeToDestination}
                        </Grid>
                        <Grid item xs={12}>
                            {this.props.text.distance} {flightData.destinationIATA}{' '}
                            {flightData.distanceToDestination} km
                        </Grid>
                    </FlightInfoGrid>
                </CardContent>
            </FlightCard>
        )
    }
}
