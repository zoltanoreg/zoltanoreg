import { number, select, text } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import * as React from 'react'
import { FlightInfo } from '../src/flightInfo'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import { blue, red } from '@material-ui/core/colors'
import { JssProvider } from 'react-jss'
import { generateClassName, jss } from './config/jss-config'

const theme = createMuiTheme({
    palette: {
        primary: red,
        secondary: blue,
    },
    typography: {
        allVariants: {
            color: '#14406e',
            fontSize: '1rem',
            fontWeight: 100,
            whiteSpace: 'nowrap',
        },
        htmlFontSize: 12,
        useNextVariants: true,
    },
})

storiesOf('FlightInfo', module)
    .add('Flight is at the strat', () => (
        <MuiThemeProvider theme={theme}>
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightInfo
                    flightData={{
                        destinationIATA: text('destinationIATA', 'BUD'),
                        distanceToDestination: number('distanceToDestination (km)', 100),
                        distanceToOrigin: number('distanceToOrigin (km)', 0),
                        originIATA: text('originIATA', 'LAX'),
                        timeToDestination: number('timeToDestination (minutes)', 120),
                    }}
                    text={{
                        title: text('Title text', 'Flight info'),
                        subtitle: text('Subtitle text', 'We are on our way'),
                        from: text('From text', 'From'),
                        to: text('To text', 'To'),
                        arriving: text('Arriving text', 'Arriving'),
                        distance: text('Distance text', 'Distance'),
                    }}
                />
            </JssProvider>
        </MuiThemeProvider>
    ))
    .add('Flight is at halfway', () => (
        <MuiThemeProvider theme={theme}>
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightInfo
                    flightData={{
                        destinationIATA: text('destinationIATA', 'BUD'),
                        distanceToDestination: number('distanceToDestination (km)', 100),
                        distanceToOrigin: number('distanceToOrigin (km)', 100),
                        originIATA: text('originIATA', 'LAX'),
                        timeToDestination: number('timeToDestination (minutes)', 120),
                    }}
                    text={{
                        title: text('Title text', 'Flight info'),
                        subtitle: text('Subtitle text', 'We are on our way'),
                        from: text('From text', 'From'),
                        to: text('To text', 'To'),
                        arriving: text('Arriving text', 'Arriving'),
                        distance: text('Distance text', 'Distance'),
                    }}
                />
            </JssProvider>
        </MuiThemeProvider>
    ))
    .add('Flight is at the end', () => (
        <MuiThemeProvider theme={theme}>
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightInfo
                    flightData={{
                        destinationIATA: text('destinationIATA', 'BUD'),
                        distanceToDestination: number('distanceToDestination (km)', 0),
                        distanceToOrigin: number('distanceToOrigin (km)', 100),
                        originIATA: text('originIATA', 'LAX'),
                        timeToDestination: number('timeToDestination (minutes)', 120),
                    }}
                    text={{
                        title: text('Title text', 'Flight info'),
                        subtitle: text('Subtitle text', 'We are on our way'),
                        from: text('From text', 'From'),
                        to: text('To text', 'To'),
                        arriving: text('Arriving text', 'Arriving'),
                        distance: text('Distance text', 'Distance'),
                    }}
                />
            </JssProvider>
        </MuiThemeProvider>
    ))
    .add('Long texts', () => (
        <MuiThemeProvider theme={theme}>
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightInfo
                    flightData={{
                        destinationIATA: text('destinationIATA', 'BUD'),
                        distanceToDestination: number('distanceToDestination (km)', 0),
                        distanceToOrigin: number('distanceToOrigin (km)', 100),
                        originIATA: text('originIATA', 'LAX'),
                        timeToDestination: number('timeToDestination (minutes)', 120),
                    }}
                    text={{
                        title: text('Title text', 'Flight info lorem psum dolor'),
                        subtitle: text('Subtitle text', 'We are on our way lorem psum dolor'),
                        from: text('From text', 'From lorem psum dolor'),
                        to: text('To text', 'To lorem psum dolor'),
                        arriving: text('Arriving text', 'Arriving lorem psum dolor'),
                        distance: text('Distance text', 'Distance lorem psum dolor'),
                    }}
                />
            </JssProvider>
        </MuiThemeProvider>
    ))
